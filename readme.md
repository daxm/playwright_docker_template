# Playwright Web Automation Docker Template

# Setup and Test
1. Clone repo
2. run `runme.sh`
3. If it runs successfully the output should end with:
    ```bash
    playwright        | Fast and reliable end-to-end testing for modern web apps | Playwright
    playwright exited with code 0
    ```

# Programming
1. Don't rename `./app/bootstrap.py` but start your code in there.
2. Code as needed.

