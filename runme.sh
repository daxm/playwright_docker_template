#!/usr/bin/env bash

echo "--> Stopping any running instances"
docker-compose down
echo

echo "--> Updating GIT repo"
git pull
echo

echo "--> Validate existence of .env file.  If not copy from .env-template."
if [ ! -f .env ]
then
  printf "\t.env file doesn't exist; creating it from .env-template."
  cp .env-template .env
fi
echo

echo "--> Pulling Docker images:"
docker-compose pull
echo

echo "--> Start docker-compose build and launch containers if successful:"
docker-compose up --remove-orphans --build
echo
